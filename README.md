## Todo List
![](https://www.javanx.cn/wp-content/themes/lensnews2.2/images/post/20190522112534.png)

如果您所在公司有用过类似的todolist产品，那应该对其不是很陌生。todolist（待办事项列表），非常有名的todolist产品有Teambition，JIRA等等。Teambition是收费的，而且价格也不是很便宜啦（但是人家的功能确实还可以，可谓非常强大）。所以，自己倒腾一个简单的，有兴趣的同学可以持续关注。

> 之前有过一个相同的项目 [https://github.com/javanf/todo-list](https://github.com/javanf/todo-list)，所以这次项目也算第二个版本，也是会做到尽善尽美。



## 项目插件

- [Vite](https://github.com/vitejs/vite)

- [Electron](https://github.com/electron/electron)

- [TypeScript](https://github.com/microsoft/TypeScript)

- [vite-plugin-electron](https://github.com/electron-vite/vite-plugin-electron)

等等

## 功能规划
* 1、初始化项目

* 2、Todo List UI框架

  * 2.1、个人本地存储简易任务
  * 2.2、Vue.Draggable拖拽任务
  * 2.3、新增任务UI
  * 2.4、任务优先级、定时提醒
  * 2.5、...

* 3、数据入库，接口调试
  * 3.1、Express搭建后台服务
  * 3.2、Todo List数据库设计
  * 3.3、Sequelize连接数据，接口书写
  * 3.4、Api Doc 生成接口文档
  * 3.5、log4j 日志管理
  * 3.6、...

* 4、Socket.io多人协同

* 5、发布

以上规划可能存在交集，以及补充，看项目实际进展。

## 项目地址
* [https://gitee.com/javanx/todolist](https://gitee.com/javanx/todolist)
* [https://gitee.com/javanx/todolist-server](https://gitee.com/javanx/todolist-server)